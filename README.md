The IGB App Merge Annotation Track makes a new track by combining data from two or more selected tracks.  

This is useful when you want to save space in the display. For example, you may have several biological replicates from an experiment. Merging data from the same sample type can make comparisons easier.

Once you install this App, IGB adds a new menu item to the **Multi-Track** section of the **Operations** section of the **Annotation** tab in IGB.

To run the operator:

* Go to the **Annotation** tabbed panel.
* Select two or more annotation tracks by SHIFT-clicking their track labels.

Note that the **Multi-track Operations** menu is now active.

* Select **Merge** option to create a new track with data from the selected tracks.
